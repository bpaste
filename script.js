// Stolen from VNDB

function byId(n) {
  return document.getElementById(n)
}

function tag() {
  if(arguments.length == 1)
    return typeof arguments[0] != 'object' ? document.createTextNode(arguments[0]) : arguments[0];
  var el = typeof document.createElementNS != 'undefined'
    ? document.createElementNS('http://www.w3.org/1999/xhtml', arguments[0])
    : document.createElement(arguments[0]);
  for(var i=1; i<arguments.length; i++) {
    if(arguments[i] == null)
      continue;
    if(typeof arguments[i] == 'object' && !arguments[i].appendChild) {
      for(attr in arguments[i]) {
        if(attr == 'style')
          el.setAttribute(attr, arguments[i][attr]);
        else
          el[ attr == 'class' ? 'className' : attr == 'for' ? 'htmlFor' : attr ] = arguments[i][attr];
      }
    } else
      el.appendChild(tag(arguments[i]));
  }
  return el;
}

function addBody(el) {
  if(document.body.appendChild)
    document.body.appendChild(el);
  else if(document.documentElement.appendChild)
    document.documentElement.appendChild(el);
  else if(document.appendChild)
    document.appendChild(el);
}

function setCookie(n,v) {
  var date = new Date();
  date.setTime(date.getTime()+(365*24*60*60*1000));
  document.cookie = n+'='+v+'; expires='+date.toGMTString()+'; path=/';
}

function getCookie(n) {
  var l = document.cookie.split(';');
  for(var i=0; i<l.length; i++) {
    var c = l[i];
    while(c.charAt(0) == ' ')
      c = c.substring(1,c.length);
    if(c.indexOf(n+'=') == 0)
      return c.substring(n.length+1,c.length);
  }
  return null;
}

//////

function setsyn(w) {
  byId('s').value = w.textContent;
  byId('s').focus();
  return false;
}

function showall() {
  byId('syntax').style.display = byId('syntax').style.display == 'none' ? '' : 'none';
  return false;
}

function promptPass(t,u,s,b) {
  var close = function () { byId('pp').parentNode.removeChild(byId('pp')); return false; };
  if(byId('pp')) {
    if(byId('pp').pp_u == u)
      return close();
    else
      close();
  }
  addBody(tag('div', { id: 'pp', pp_u: u }, tag('form', { method: 'post', action: u },
    t, tag('br', null),
    tag('input', { type: 'password', name: 'pc', id: 'pc', value: getCookie('secret_passcode') || '' }), tag('br', null),
    s ? tag('input', { type: 'checkbox', name: 'psp', id: 'psp', value: 1 }) : '',
    s ? tag('label', { 'for': 'psp' }, ' save on my computer') : '',
    s ? tag('br', null) : '',
    tag('input', { type: 'submit', value: b, 'class': 'ppbut' }),
    tag('input', { type: 'button', value: 'cancel', 'class': 'ppbut', onclick: close })
  )));
  return false;
}

function unpaste(u) {
  return promptPass('Please enter your passcode to delete this paste.', u, false, 'unpaste!');
}

function mypastes() {
  return promptPass('Please enter your passcode to see your pastes.', '/mypastes', true, 'show');
}


// Copy passcode from cookie to passcode field
var x = byId('p');
if(x) {
  x.value = getCookie('secret_passcode') || '';
}

// Load default formatting
x = byId('formatsave');
if(x) {
  var w = byId('w');
  var c = byId('c');
  var s = byId('s');
  var def = function() {
    var cook = getCookie('formatdef');
    if(!cook)
      return [w.checked, c.checked, s.value];
    else {
      var v = cook.split(/,/); // wrap, click, syntax, secure (not used anymore)
      v[0] = v[0]=='1' ? true : false;
      v[1] = v[1]=='1' ? true : false;
      return v;
    }
  };
  var setclass = function() {
    x.className = w.checked==v[0] && c.checked==v[1] && s.value==v[2] ? '' : 'notdef';
  };
  var v = def();
  if(!byId('p').value) { // Don't apply some settings when copying another paste
    w.checked = v[0];
    c.checked = v[1];
    s.value = v[2];
  }
  x.onclick = function() {
    setCookie('formatdef', (w.checked ? '1' : '0')+','+(c.checked ? '1' : '0')+','+s.value+','+(l.checked ? '1' : '0'));
    v = def();
    setclass();
    return false;
  };
  w.onclick = c.onclick = s.onkeyup = s.onfocus = setclass;
  setclass();
}

